# Copyright (c) 2013  Peter Pentchev
# All rights reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SCRIPTS=	cawk ccsvtool ccut
MANPAGES=	cawk.1 ccsvtool.1 ccut.1
MANPAGESZ=	$(subst .1,.1.gz,${MANPAGES})

PREFIX?=	/usr/local
BINDIR?=	${PREFIX}/bin
MANDIR?=	${PREFIX}/man
MAN1DIR?=	${MANDIR}/man1

BINOWN?=	root
BINGRP?=	root
BINMODE?=	755

SHAREOWN?=	${BINOWN}
SHAREGRP?=	${BINGRP}
SHAREMODE?=	644

GZIP?=		gzip -9
INSTALL?=	install
MKDIR?=		mkdir -p
RM?=		rm -f

COPY?=		-c

INSTALL_SCRIPT?=	${INSTALL} ${COPY} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_DATA?=	${INSTALL} ${COPY} -o ${SHAREOWN} -g ${SHAREGRP} -m ${SHAREMODE}

all:		${SCRIPTS} ${MANPAGESZ}

clean:
		${RM} ${MANPAGESZ}

%.1.gz:		%.1
		${GZIP} -c "$<" > "$@" || (${RM} "$@"; false)

install:	all install-scripts install-manpages

install-scripts:	${SCRIPTS}
		set -e; \
		${MKDIR} "${DESTDIR}${BINDIR}"; \
		for i in ${SCRIPTS}; do \
			${INSTALL_SCRIPT} "$$i" "${DESTDIR}${BINDIR}"; \
		done

install-manpages:	${MANPAGESZ}
		set -e; \
		${MKDIR} "${DESTDIR}${MAN1DIR}"; \
		for i in ${MANPAGESZ}; do \
			${INSTALL_DATA} "$$i" "${DESTDIR}${MAN1DIR}"; \
		done

test:		all
		prove t

.PHONY:		all clean install install-scripts install-manpages test
