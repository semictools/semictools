#!/bin/sh
#
# Copyright (c) 2013  Peter Pentchev
# All rights reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

echo '1..13'

[ -z "$CCSVTOOL" ] && CCSVTOOL='sh ccsvtool'
[ -z "$CSVTEST" ] && CSVTEST='testdata.csv'

echo '# ccsvtool with no arguments should exit with code 2'
v=`$CCSVTOOL > /dev/null 2>&1`
res="$?"
if [ "$res" = 2 ]; then echo 'ok 1'; else echo "not ok 1 exit code $res"; fi

echo '# The first field of the first line should be "GameId"'
v=`head -n1 "$CSVTEST" | $CCSVTOOL col 1 -`
res="$?"
if [ "$res" = 0 ]; then echo 'ok 2'; else echo "not ok 2 exit code $res"; fi
if [ "$v" = 'GameId' ]; then echo 'ok 3'; else echo "not ok 3 v $v"; fi

echo '# The DlType of the first "real" game should be "linux"'
v=`$CCSVTOOL -u \; head 2 "$CSVTEST" | $CCSVTOOL -u \; drop 1 - | $CCSVTOOL col 4 -`
res="$?"
if [ "$res" = 0 ]; then echo 'ok 4'; else echo "not ok 4 exit code $res"; fi
if [ "$v" = 'linux' ]; then echo 'ok 5'; else echo "not ok 5 v $v"; fi

echo '# Find out if csvtool outputs the header in namedcol'
unset header
if $CCSVTOOL namedcol DlClass "$CSVTEST" | fgrep -qe DlClass; then
	header=1
fi

echo '# Get the number of game lines in the test data file'
nlines=`wc -l "$CSVTEST" | awk '{print $1}'`
res="$?"
if [ "$res" = 0 ]; then echo 'ok 6'; else echo "not ok 6 exit code $res"; fi
if expr "x$nlines" : 'x[1-9][0-9]*$' > /dev/null; then echo 'ok 7'; else echo "not ok 7 nlines '$nlines'"; fi
echo "# - got nlines $nlines"
if [ -z "$header" ]; then
	nlines=`expr "$nlines" - 1`
	echo "# - it seems that there are $nlines game descriptions there"
else
	echo '# - header row detected, not decreasing nlines'
fi
nlinesm1=`expr "$nlines" - 1`
echo "# - and one less is $nlinesm1"

echo '# There should be one DlType/DlClass combo per line'
v=`$CCSVTOOL namedcol DlType,DlClass "$CSVTEST" | wc -l`
res="$?"
if [ "$res" = 0 ]; then echo 'ok 8'; else echo "not ok 8 exit code $res"; fi
if [ "$v" = "$nlines" ]; then echo 'ok 9'; else echo "not ok 9 v $v"; fi

echo '# And there should be one less distinct DlType/DlClass combo (one repeated)'
v=`$CCSVTOOL namedcol DlType,DlClass "$CSVTEST" | sort -u | wc -l`
res="$?"
if [ "$res" = 0 ]; then echo 'ok 10'; else echo "not ok 10 exit code $res"; fi
if [ "$v" = "$nlinesm1" ]; then echo 'ok 11'; else echo "not ok 11 v $v"; fi

# Bah, in this case we've already tested this, but oh well
echo '# let us see if ccsvtool accepts other options, same as csvtool'
v=`head -n3 "$CSVTEST" | tail -n1 | $CCSVTOOL  -u ':' -z col 3 -`
if [ "$res" = 0 ]; then echo 'ok 12'; else echo "not ok 12 exit code $res"; fi
if [ "$v" = 'mac' ]; then echo 'ok 13'; else echo "not ok 13 v $v"; fi
