#!/bin/sh
#
# Copyright (c) 2013  Peter Pentchev
# All rights reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

echo '1..13'

[ -z "$CAWK" ] && CAWK='sh cawk'
[ -z "$CSVTEST" ] && CSVTEST='testdata.csv'

echo '# cawk with no program should exit with code 0'
v=`$CAWK '' > /dev/null 2>&1`
res="$?"
if [ "$res" = 0 ]; then echo 'ok 1'; else echo "not ok 1 exit code $res"; fi

echo '# The first field of the first line should be "GameId"'
v=`head -n1 "$CSVTEST" | $CAWK '{print $1}'`
res="$?"
if [ "$res" = 0 ]; then echo 'ok 2'; else echo "not ok 2 exit code $res"; fi
if [ "$v" = 'GameId' ]; then echo 'ok 3'; else echo "not ok 3 v $v"; fi

echo '# The DlType of the first "real" game should be "linux"'
v=`$CAWK 'NR > 1 { print $4; exit; }' "$CSVTEST"`
res="$?"
if [ "$res" = 0 ]; then echo 'ok 4'; else echo "not ok 4 exit code $res"; fi
if [ "$v" = 'linux' ]; then echo 'ok 5'; else echo "not ok 5 v $v"; fi

echo '# Get the number of game lines in the test data file'
nlines=`wc -l "$CSVTEST" | awk '{print $1}'`
res="$?"
if [ "$res" = 0 ]; then echo 'ok 6'; else echo "not ok 6 exit code $res"; fi
if expr "x$nlines" : 'x[1-9][0-9]*$' > /dev/null; then echo 'ok 7'; else echo "not ok 7 nlines '$nlines'"; fi
nlines=`expr "$nlines" - 1`
echo "# - it seems that there are $nlines game descriptions there"
nlinesm1=`expr "$nlines" - 1`
echo "# - and one less is $nlinesm1"

echo '# There should be one DlType/DlClass combo per line'
v=`$CAWK 'NR == 1 { \
		for (i = 1; i <= NF; i++) { \
			if ($i == "DlType") { \
				dltype = i \
			} else if ($i == "DlClass") { \
				dlclass = i \
			} \
		} \
	} \
	NR > 1 { print $dltype " " $dlclass }' "$CSVTEST" | wc -l`
res="$?"
if [ "$res" = 0 ]; then echo 'ok 8'; else echo "not ok 8 exit code $res"; fi
if [ "$v" = "$nlines" ]; then echo 'ok 9'; else echo "not ok 9 v $v"; fi

echo '# And there should be one less distinct DlType/DlClass combo (one repeated)'
v=`$CAWK 'NR == 1 { \
		for (i = 1; i <= NF; i++) { \
			if ($i == "DlType") { \
				dltype = i \
			} else if ($i == "DlClass") { \
				dlclass = i \
			} \
		} \
	} \
	NR > 1 { print $dltype " " $dlclass }' "$CSVTEST" | sort -u | wc -l`
res="$?"
if [ "$res" = 0 ]; then echo 'ok 10'; else echo "not ok 10 exit code $res"; fi
if [ "$v" = "$nlinesm1" ]; then echo 'ok 11'; else echo "not ok 11 v $v"; fi

echo '# ok, let us see if cawk accepts other options, same as awk'
v=`$CAWK -v 'fname=airforte_v1_1.tar.gz' -v OFS=: '$6 == fname { print $1, $2 }' "$CSVTEST"`
res="$?"
if [ "$res" = 0 ]; then echo 'ok 12'; else echo "not ok 12 exit code $res"; fi
if [ "$v" = "airforte:Air Forte" ]; then echo 'ok 13'; else echo "not ok 13 v $v"; fi
